#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2021, 2022, 2023
# License: All content is licensed under the terms of <the MIT License>
# Developed on: Debian 11.x; macOS Ventura x86 architecture
# Tested on: Debian 11.x; macOS Ventura x86 architecture
# 
# By default, sphinx-build supports a number of outputs. The ones I’m most likely to use are the following:

# html: to make standalone HTML files
# dirhtml: to make HTML files named index.html in directories
# singlehtml: to make a single large HTML file
# epub: to make an epub
# latexpdf: to make LaTeX and PDF files (default pdflatex)
# text: to make text files
# man: to make manual pages
# gettext: to make PO message catalogs
# doctest: to run all doctests embedded in the documentation (if enabled)
# coverage: to run coverage check of the documentation (if enabled)
# And of those, let’s face it, it’s dirhtml, epub, latexpdf. If you’re building towards a constrained set of 
# targets, you can usually more easily work on styling them and using the particular affordances those formats offer.
# this build process relies heavily on conf.py

# Exit on error. Append "|| true" if you expect an error.
# set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}

readonly USERSCRIPT="cloud"
echo "${USERSCRIPT}"

DESTINATION="/Users/${USERSCRIPT}/repos/"
echo "${DESTINATION}"

EXECUTIONPATH="${DESTINATION}"
echo "${EXECUTIONPATH}"

n="ndaal_public_schriftliche_stellungnahme_hitsig"
echo "${n}"

readonly UPDATEINSTALL="True1"
echo "The variable UPDATEINSTALL is set to ${UPDATEINSTALL}"

readonly UPDATESTATIC="True"
echo "The variable UPDATESTATIC is set to ${UPDATESTATIC}"

DIRECTORY="${EXECUTIONPATH}${n}/documentation/HITSiG_all/"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if $DIRECTORY doesn't exist.
    mkdir -p -v "${DIRECTORY}"
fi

rm -v "${DIRECTORY}"* || true

cd "${DESTINATION}${n}/documentation/source/" || exit
echo "change to directory ${DESTINATION}${n}/documentation/source/"

# robots.txt file
# https://developers.google.com/search/docs/advanced/robots/create-robots-txt
# placed at  ../documentation/source/robots.txt
# https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/blob/master/documentation/source/robots.txt
#
# in the directory .../documentation/source/_test/ are placed further documentation
# Sphinx-Documentation.pdf
# nbsphinx.pdf
#
# the actual version of this bash script resides at:
#
# https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/blob/master/documentation/create.sh
#
# the directory _branding consists of branding material like Logo in different variations to put in _static
# but keep in mind that Logo.png of ndaal still is in _static and a copy activity is not needed
#
# Attention: svg graphic files are typically not rendered in pdf and will cause a termination of make html
#
# the directory _legal consists of common information and general terms and conditions
#
# after make html a sitemap.xml is created and placed
# ../documentation/build/html/
# example here
# https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/blob/master/documentation/build/html/sitemap.xml

Function_Update_Install_Requirements () {
  # install all software mentioned in requirements.txt with pip3
  # the actual version of this file resides at:
  # https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/blob/master/documentation/source/requirements.txt
  pip3 install -r requirements.txt || true
  # pip3 freeze > requirements_fail_save.txt
  # pip3 install -r requirements_fail_save.txt || true
  # Edit requirements.txt, and replace all ‘==’ with ‘>=’. Use the ‘Replace All’ command in the editor.
  # Upgrade all outdated packages: 

  pip3 install -r requirements.txt --upgrade || true

  # these oneliner upgrades all outdated pip3 installed packages of the system
  pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install -U || true

  # in directory _test are the test files for checking the render and building process
  # the files are placed at .../documentation/source/_test/
  # https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/tree/master/documentation/source/_test
}
#
# hints for the status
#
## Jupyther Notebooks are not working at the moment
##
## Markdown is working
## testfile for markdown https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/blob/master/documentation/source/_test/testmarkdowninSphinx.md
##
## RST is working
## testfile for rst https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/blob/master/documentation/source/_test/test.rst
#
# an example based on arc42 is placed under 
# https://gitlab.com/ndaal_team_developer/ndaal_ml_infra_deploy/-/tree/master/documentation/source/_example
#
# footnotes are not positioned well
#

if [ "${UPDATEINSTALL}" == "True" ]
then
  echo "The variable UPDATEINSTALL is set to ${UPDATEINSTALL}"

  Function_Update_Install_Requirements

else
  echo "We are doing no installation or update of the requirements defined in requirements.txt"
fi

DIRECTORY="${DESTINATION}${n}/documentation/source/"
echo "${DIRECTORY}"

if [ -d "${DIRECTORY}" ]; then
    # Control will enter here if $DIRECTORY doesn't exist.
    mkdir -p -v "${DIRECTORY}/old"
fi

SOURCE="/Users/${USERSCRIPT}/repos/ndaal_public_schriftliche_stellungnahme_hitsig/documentation/source/"
echo "${SOURCE}"

DESTINATION="/Users/${USERSCRIPT}/repos/ndaal_public_schriftliche_stellungnahme_hitsig/documentation/source/"
echo "${DESTINATION}"

Function_Create_Directories () {
# create directories for ../_static
for n in "${array[@]}"
do
    (
    echo "Create Directory ${n}/_static"
    DIRECTORY="${SOURCE}_${n}/"
    
    if [ ! -d "${DIRECTORY}" ]; then
      (
      # Control will enter here if $DIRECTORY doesn't exist.
      echo "Create Directory ${SOURCE}_${n}/ because it doesn't exist."
      mkdir -p -v "${DIRECTORY}"
      )
    fi
    )
done
}

Function_Emptying_Directories () {
echo "remove _static/* files to prevent duplettes"
for n in "${array[@]}"
do
    (
    echo "Emptying Directory ${n}"
    DIRECTORY="${SOURCE}_${n}/"
    
    if [ -d "${DIRECTORY}" ]; then
      (
      # Control will enter here if $DIRECTORY exist.
      echo "emptying of directory ${SOURCE}_${n}/ in progress"
      rm -v "${DIRECTORY}*" || true
      )
    fi
    )
done
}

Function_Copy_Content () {
for n in "${array[@]}"
do
    (
    echo "Copy Content ../_static to ${n}"
    DIRECTORY="_${n}"
    
    if [ -d "${DIRECTORY}" ]; then
      (
      # Control will enter here if $DIRECTORY exist.
      echo "${DIRECTORY}"
      echo "${DESTINATION}${DIRECTORY} in progress"
      echo "${SOURCE} is Source"
      echo "${SOURCE}_static/ is Source with _static"
      xcp -v ${SOURCE}_static/* ${DESTINATION}${DIRECTORY}
      )
    fi
    )
done
}

Function_Remove_old_Files () {
	(
	# this is more secure and robost instead of rm ./*
	cd "${DESTINATION}" || exit
	echo "Remove old Files"
	rm -v ./*.md5 || true
	rm -v ./*.sha256 || true
	rm -v ./*.sha512 || true
	cd "${SOURCE}" || exit
	)
}

Function_Substitute_Pattern () {
echo "replace *.rst files jpeg and jpg with png"
for n in "${substitute[@]}"
do
    (    
    DIRECTORY="${SOURCE}_${n}/"
    echo "${DIRECTORY}"
    cd "${DIRECTORY}" || exit
    echo "${DIRECTORY}"
    echo "replace in *.rst files jpeg and jpg with png ${n}"
    sed -i '' 's/(.jpeg)/(.png)/g' ./*.rst || true
    sed -i '' 's/(.jpg)/(.png)/g' ./*.rst || true
    sed -i '' 's/(.jpeg)/(.png)/g' ./*.txt || true
    sed -i '' 's/(.jpg)/(.png)/g' ./*.txt || true
    echo "replace in *.rst files svg with png ${n}"
    sed -i '' 's/(.svg)/(.png)/g' ./*.rst || true
    echo "replace in *.rst rubric with Rubric ${n}"
    sed -i '' 's/(.. rubric::)/(.. Rubric::)/g' ./*.rst || true
    )
done
}

cd "${SOURCE}" || exit

# declare an array called array
declare -a array
array=(
"document/_static"
"legal/_static"
)

# declare an array called array
declare -a substitute
substitute=(
"document"
"legal"
)
if [ "${UPDATESTATIC}" == "True" ]
then
  echo "The variable UPDATESTATIC is set to ${UPDATESTATIC}"

  Function_Remove_old_Files

  Function_Create_Directories

  Function_Copy_Content

else
  echo "We are doing no installation or update of the requirements defined in requirements.txt"
fi

Function_Substitute_Pattern



DESTINATION="/Users/${USERSCRIPT}/repos/"
echo "${DESTINATION}"

EXECUTIONPATH="${DESTINATION}"
echo "${EXECUTIONPATH}"

n="ndaal_public_schriftliche_stellungnahme_hitsig"
echo "${n}"

DIRDATE="$(date +"%Y-%m-%d")"
echo "${DIRDATE}"


cd "${DESTINATION}${n}/documentation/" || exit
echo "change to directory ${DESTINATION}${n}/documentation/" || exit

echo "cleanup some files like *.txt.md5"
find . -name "*.txt.md5" -exec rm -rf {} \; || true
echo "cleanup some files like *.rst.md5"
find . -name "*.rst.md5" -exec rm -rf {} \; || true

echo "copy ${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/imgs/* to each "
xcp -v "/Users/${USERSCRIPT}/repos/ndaal_ml_infra_deploy/documentation/source/imgs/"* "${DESTINATION}${n}/documentation/source/imgs/"

cd "${DESTINATION}${n}/documentation/" || exit
echo "change to directory ${DESTINATION}${n}/documentation/" || exit

make clean || exit
make html || true
make singlehtml || true
make epub || true
make htmlhelp || true
make markdown || true
make latexpdf || true


cd "${DESTINATION}${n}/documentation/build/singlehtml/" || exit
echo "change to directory ${DESTINATION}${n}/documentation/build/singlehtml/"

# https://pandoc.org/
# Pandoc Installation
# macOS
# brew install pandoc
# Linux Debian
# sudo apt-get install pandoc
#
# convert singlehtml to Word
pandoc -s index.html -t docx -o "HITSiG_-_Schriftliche_Stellungnahme_Pierre_Gronau.docx"
# convert singlehtml to OpenDocument (LibreOffice)
pandoc -s index.html -t odt -o "HITSiG_-_Schriftliche_Stellungnahme_Pierre_Gronau.odt"

#find "${EXECUTIONPATH}${n}" -type f -name "*.rst" -exec sh -c "md5sum {} | awk '{print \$1}' | tee {}.md5" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.rst" -exec sh -c "sha256sum {} | awk '{print \$1}' | tee {}.sha256" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.rst" -exec sh -c "sha512sum {} | awk '{print \$1}' | tee {}.sha512" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.rst" -exec sh -c "sha3-512sum {} | awk '{print \$1}' | tee {}.sha3-512" \;
# cargo install b2sum
find "${EXECUTIONPATH}${n}" -type f -name "*.rst" -exec sh -c "b2sum {} | awk '{print \$1}' | tee {}.blake2b" \;
# cargo install b3sum
find "${EXECUTIONPATH}${n}" -type f -name "*.rst" -exec sh -c "b3sum {} | awk '{print \$1}' | tee {}.blake3" \;

#find "${EXECUTIONPATH}${n}" -type f -name "*.txt" -exec sh -c "md5sum {} | awk '{print \$1}' | tee {}.md5" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.txt" -exec sh -c "sha256sum {} | awk '{print \$1}' | tee {}.sha256" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.txt" -exec sh -c "sha512sum {} | awk '{print \$1}' | tee {}.sha512" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.txt" -exec sh -c "sha3-512sum {} | awk '{print \$1}' | tee {}.sha3-512" \;
# cargo install b2sum
find "${EXECUTIONPATH}${n}" -type f -name "*.txt" -exec sh -c "b2sum {} | awk '{print \$1}' | tee {}.blake2b" \;
# cargo install b3sum
find "${EXECUTIONPATH}${n}" -type f -name "*.txt" -exec sh -c "b3sum {} | awk '{print \$1}' | tee {}.blake3" \;

#find "${EXECUTIONPATH}${n}" -type f -name "*.pdf" -exec sh -c "md5sum {} | awk '{print \$1}' | tee {}.md5" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.pdf" -exec sh -c "sha256sum {} | awk '{print \$1}' | tee {}.sha256" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.pdf" -exec sh -c "sha512sum {} | awk '{print \$1}' | tee {}.sha512" \;
find "${EXECUTIONPATH}${n}" -type f -name "*.pdf" -exec sh -c "sha3-512sum {} | awk '{print \$1}' | tee {}.sha3-512" \;
# cargo install b2sum
find "${EXECUTIONPATH}${n}" -type f -name "*.pdf" -exec sh -c "b2sum {} | awk '{print \$1}' | tee {}.blake2b" \;
# cargo install b3sum
find "${EXECUTIONPATH}${n}" -type f -name "*.pdf" -exec sh -c "b3sum {} | awk '{print \$1}' | tee {}.blake3" \;

# fd "${EXECUTIONPATH}${n}" -type f -name "*.pdf" -exec sh -c "b3sum {} | awk '{print \$1}' | tee {}.blake3" \;

cd "${EXECUTIONPATH}" || true
echo "${EXECUTIONPATH}"

cp -p -v ${EXECUTIONPATH}${n}/documentation/build/singlehtml/*.odt "${EXECUTIONPATH}${n}/documentation/HITSiG_all/" || true
cp -p -v ${EXECUTIONPATH}${n}/documentation/build/singlehtml/*.docx "${EXECUTIONPATH}${n}/documentation/HITSiG_all/" || true
cp -p -v ${EXECUTIONPATH}${n}/documentation/build/latex/ndaal-pierregronau-hitsig.pdf "${EXECUTIONPATH}${n}/documentation/HITSiG_all/HITSiG_-_Schriftliche_Stellungnahme_Pierre_Gronau.pdf" || true

exiftool -n "${EXECUTIONPATH}${n}/documentation/HITSiG_all/"* || true
exiftool -all= -overwrite_original "${EXECUTIONPATH}${n}/documentation/HITSiG_all/"* || true
exiftool -n "${EXECUTIONPATH}${n}/documentation/HITSiG_all/"* || true



script_name1="basename ${0}"
script_path1="$(dirname "$(readlink -f "${0}")")"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
