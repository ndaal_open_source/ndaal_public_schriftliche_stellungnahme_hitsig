\selectlanguage *{english}
\contentsline {chapter}{\numberline {1}Einführung}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Zu den Vorschriften im Einzelnen}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}§ 2 Nr. 1 HITSiG\sphinxhyphen {}E – Begriffsbestimmungen}{4}{section.2.1}%
\contentsline {section}{\numberline {2.2}§ 2 Nr. 2 HITSiG\sphinxhyphen {}E – Begriffsbestimmungen}{4}{section.2.2}%
\contentsline {section}{\numberline {2.3}§ 2 Nr. 3 HITSiG\sphinxhyphen {}E – Begriffsbestimmungen}{4}{section.2.3}%
\contentsline {section}{\numberline {2.4}§ 2 Nr. 4 HITSiG\sphinxhyphen {}E – Begriffsbestimmungen}{5}{section.2.4}%
\contentsline {section}{\numberline {2.5}§ 3 HITSiG\sphinxhyphen {}E – Grundsätze der Informationssicherheit}{5}{section.2.5}%
\contentsline {section}{\numberline {2.6}§ 4 HITSiG\sphinxhyphen {}E – Die oder der Zentrale Informationssicherheitsbeauftragte der Landesverwaltung}{6}{section.2.6}%
\contentsline {section}{\numberline {2.7}§ 5 HITSiG\sphinxhyphen {}E – Zentrum für Informationssicherheit}{6}{section.2.7}%
\contentsline {section}{\numberline {2.8}§ 6 HITSiG\sphinxhyphen {}E – Zentraler IT\sphinxhyphen {}Dienstleister des Landes}{6}{section.2.8}%
\contentsline {section}{\numberline {2.9}§ 7 HITSiG\sphinxhyphen {}E – Datenverarbeitung}{7}{section.2.9}%
\contentsline {section}{\numberline {2.10}§ 8 HITSiG\sphinxhyphen {}E – Verwendung von auf informationstechnischen Systemen gespeicherten Daten}{7}{section.2.10}%
\contentsline {section}{\numberline {2.11}§ 10 HITSiG\sphinxhyphen {}E – Auswertung ohne Inhaltsdaten}{8}{section.2.11}%
\contentsline {section}{\numberline {2.12}§ 11 HITSiG\sphinxhyphen {}E – Auswertung von Inhaltsdaten}{8}{section.2.12}%
\contentsline {section}{\numberline {2.13}§ 14 HITSiG\sphinxhyphen {}E – Gewährleistung der Informationssicherheit und des Datenschutzes}{8}{section.2.13}%
\contentsline {section}{\numberline {2.14}§ 16 HITSiG\sphinxhyphen {}E – Wiederherstellung der Sicherheit oder Funktionsfähigkeit informationstechnischer Systeme in herausgehobenen Fällen der Beeinträchtigung}{9}{section.2.14}%
\contentsline {section}{\numberline {2.15}§ 17 HITSiG\sphinxhyphen {}E – Information der Betroffenen}{9}{section.2.15}%
\contentsline {chapter}{Kontakt}{10}{section*.22}%
