******************************************************************************
ndaal - General Information
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. meta::
   :keywords: ndaal sphinx documentation generator
   :keywords lang=en: IT Compliance IT Security Automation Common Information General Information
   :keywords lang=de: Allgemeine Informationen

..

.. contents:: Content - General Information
   :depth: 3

..


General Information
------------------------------------------------------------------------------

In a safety audit, the results are based on a self-assessment with the 
responsible persons and the analysis of the relevant systems, which are 
limited in scope and time horizon.
In a technical security analysis, the security-relevant information is
determined directly during the analysis. In principle, here only 
vulnerabilities can be discovered, but no proof of their absence can be
provided.
Despite the greatest possible care, it is therefore possible in both cases
that not all security aspects could be discovered by ndaal. Therefore we 
exclude any liability for existing and not detected risks.
The results of the investigation do not release the customer in any way from
the pursuit of the safety objectives. The customer is in any case responsible
for the implemented measures for the elimination of vulnerabilities and for 
the guarantee their security objectives.

..

Any liability for damage resulting from misuse of the information is excluded.

.....

Opening Clause
------------------------------------------------------------------------------

It is impossible to take into account all possible variants and factors of 
influencing in advance. The measures and minimum requirements specified in 
this document are generally to be implemented as described. There is only the 
possibility to deviate from them, reasons so require and, at the same time,
supplementary measures are taken. 
These measures SHALL be suitable to reduce the possible risks resulting from
the deviation to an acceptable level. 

..

``Irrefutable`` means that compelling technical or operational reasons are
exist. The mere fact that, for example, a smaller distance between
between data center locations enables more cost-effective operation, 
is not to be regarded as **unavoidable**.


.....

Disclaimer
------------------------------------------------------------------------------

This project is made for educational and ethical testing purposes only. 
Usage of ndaal scan for attacking targets without prior mutual consent is 
illegal. It is the end user’s responsibility to obey all applicable local, 
state and federal laws.

..

We have checked the contents of this publication for conformity with the 
hardware and software described. 
However, deviations cannot be ruled out, so that we cannot accept any 
liability for the therefore cannot accept any liability for complete
conformity. The information in this are checked regularly, and any necessary
corrections are included in subsequent editions.

.....

Rights of Use
------------------------------------------------------------------------------

The authors or ndaal grant an irrevocable, royalty-free, spatially and 
temporally unrestricted, non-exclusive right of use of the work results to the
end customer, unless otherwise regulated otherwise in the contract.

..

Claims from patents remain unaffected by this.

.....

No duty to update
------------------------------------------------------------------------------

ndaal  assumes no obligation to update any information or forward-looking 
statement contained herein, save for any information we are required to 
disclose by law.
