# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.append(os.path.abspath('.'))
# -- Project information -----------------------------------------------------

project = 'ndaal - Pierre Gronau - HITSiG'
copyright = '2023 Pierre Gronau'
author = 'Pierre Gronau'
description = 'ndaal - Schriftliche Stellungnahme - HITSiG'

# The full version, including alpha/beta/rc tags
version = '0.1.0'
release = '0.1.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # mandantory start
    'sphinxcontrib.blockdiag',
    'sphinxcontrib.actdiag',
    'sphinxcontrib.nwdiag',
    'sphinxcontrib.rackdiag',
    'sphinxcontrib.packetdiag',
    'sphinxcontrib.seqdiag',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
#    'sphinx_autopackagesummary',
#    'sphinx-autosummary-accessors',
#    'sphinx.ext.autosectionlabel',
    'sphinx.ext.coverage',
    'sphinx.ext.doctest',
    'sphinx.ext.extlinks',
    'sphinx.ext.githubpages',
    'sphinx.ext.graphviz',
    'sphinx.ext.ifconfig',
    'sphinx.ext.imgconverter',
    'sphinx.ext.imgmath',
    'sphinx.ext.inheritance_diagram',
    'sphinx.ext.intersphinx',
    'sphinx.ext.mathjax',
    # end
# https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
    'sphinx.ext.napoleon',
     'sphinx_sitemap',
# https://www.markdownguide.org/getting-started/
# https://myst-parser.readthedocs.io/en/latest/sphinx/intro.html
#    'sphinx.ext.todo',
#    'sphinx.ext.viewcode',
#    'recommonmark',
#    'sphinx_markdown_tables',
#    'sphinxcontrib.swaggerdoc',
#    'sphinxcontrib.excel_table',
#    'rusty.includesh',
    'sphinxcontrib.programoutput',
# needflow uses PlantUML and the Sphinx-extension sphinxcontrib-plantuml for generating the flows.
    'sphinxcontrib.plantuml',
#    'sphinxcontrib.datatemplates',
    'sphinxcontrib.autoyaml',
#    'sphinxcontrib.recentpages',
#    'sphinxcontrib.confluencebuilder',
# obselete 'nbsphinx',
# we use instead
#    'myst_nb',
#     'sphinx_git',
#     'variations',
#     'sphinxcontrib.openapi',
    'sphinxcontrib.details.directive',
#    'sphinx_needs',
#    'sphinxcontrib.test_reports',
# https://hieroglyph.readthedocs.io
#    'hieroglyph',
#    'sphinxcontrib.shellcheck',
#     'frigate.sphinx.ext',
#     'sphinx_charts.charts',
    #'sphinx-exec-directive',
#    'sphinx-jsonschema',
    'sphinxawesome_theme',
    'sphinxmark',
#    'sphinxcontrib-fulltoc',

#    'sphinxcontrib.mermaid',
#    'sphinxcontrib.kroki',
#   'sphinxcontrib.bibtex',
#    'sphinx_gallery.load_style',
#    'sphinx_copybutton',
# https://pypi.org/project/myst-parser/
# MyST is a rich and extensible flavor of Markdown meant for technical documentation and publishing.
    'myst_parser',
#    'sphinxcontrib-applehelp',
#    'sphinxcontrib-manpage',
#    'sphinxcontrib.dashbuilder',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'
sidebarlogo = 'Logo.png'
html_logo = 'Logo.png'
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
master_doc = 'index'
latex_engine = 'xelatex'
latex_logo = 'imgs/Logo.png'
mypreamble = '''
            \\usepackage{tikz}
            \\usepackage{lastpage}
            \\usetikzlibrary{calc}
            \\usepackage[sfdefault]{roboto}
            \\usepackage{tikzpagenodes}
            \\usepackage{url}
            \\makeatletter
            \\usepackage{fancyhdr}
            \\usepackage{stfloats}
            \\usepackage{titlesec}
            \\usepackage{enumitem}
            \\setlistdepth{99}

            \\fancypagestyle{plain}{
            \\lhead{}
            \\fancyhead[R]{\includegraphics{Logo.png}
            \\fancyhead[L]{}}
            \\fancyfoot[C]{\\textbf{Public}}}
            \\setlength{\headheight}{42pt}

            \makeatletter
            \\titleformat{\chapter}[frame]
              {\\normalfont}{\\filright\enspace \@chapapp~\\thechapter\enspace\\FrameRule=0.5pt\\FrameSep=2pt}
              {10pt}{\LARGE\\filright}
            \\titlespacing*{\chapter}
              {0pt}{0pt}{5pt}
            \makeatother

            \\ChNumVar{\Huge\\fontfamily{roboto}}
            \\ChTitleVar{\LARGE\\fontfamily{roboto}}
            \\ChTitleVar{\Large\\fontfamily{roboto}}    
            
            \geometry{
              a4paper,
              left=20mm,
              right=20mm,
              headheight=42pt,
              top=3.5cm,
              bottom=4cm,
              footskip=3cm
            }

            '''
# In case an error occurs pointing out 'customerlogo.png' &/or 'cover.png', please add the full path to the images on your local system below such that:

# \includegraphics[width=.20\textwidth]{/full/path/to/customerlogo.png} 

# \includegraphics[width=\paperwidth,height=2in]{/full/path/to/cover.png}

latex_elements = {
    'sphinxsetup': "",
    'papersize': 'a4paper',
    'releasename': "",
    'figure_align': 'htbp',
    'pointsize': '10pt',
    'extraclassoptions': 'openany,oneside',
    'preamble': mypreamble,
    # To generate a differently styled title page.
    'maketitle': r'''
    \begin{titlepage}
    \begin{tikzpicture}[remember picture,overlay]
    \draw  let \p1=($(current page.north)-(current page header area.south)$),
          \n1={veclen(\x1,\y1)} in
    node [inner sep=0,outer sep=0,below right]
          at (current page.north west){\includegraphics[width=\paperwidth,height=2in]{/Users/cloud/repos/ndaal_public_schriftliche_stellungnahme_hitsig/documentation/source/cover.png}};
    \end{tikzpicture}\\
    \vspace{6cm}\\
    \begin{tabular*}{\textwidth}{@{}l@{\extracolsep{\fill}}r@{}}
     \textbf{\Large{Schriftliche Stellungnahme}}
    &
     \includegraphics[width=.20\textwidth]{/Users/cloud/repos/ndaal_public_schriftliche_stellungnahme_hitsig/documentation/source/customerlogo.png}
    \end{tabular*}
    \vspace{0.7cm}\\
    \emph{Pierre Gronau}\\
    \vspace{0.5cm}\\
    \emph{Gesetzentwurf der Landesregierung für ein}\\
    \emph{Hessisches Gesetz zum Schutz der}\\
    \emph{elektronischen Verwaltung}\\
    \emph{(Hessisches IT-Sicherheitsgesetz – HITSiG) }\\

    \end{titlepage}
                '''}

# -- Options for Epub output ---------------------------------------------------

# Bibliographic Dublin Core info.
# filename
# epub_basename = project.replace(' ', '_') + '_' + language
epub_title = project+" "+version
epub_description = description

# Technically speaking dublincore accepts multiple author and contributor elements, but
# the sphinx builder only accepts one.
epub_author = author
epub_publisher = author
epub_copyright = copyright

# The scheme of the identifier. Typical schemes are ISBN or URL.
epub_scheme = 'URL'
# The language of the text. It defaults to the language option
# or 'en' if the language is not set.
epub_language = 'en'

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#epub_identifier = ''

# A unique identification for the text.
#epub_uid = ''

# A tuple containing the cover image and cover page html template filenames.
#epub_cover = ('_static/Cover.png')

#epub_theme = 'epub2'

# HTML files that should be inserted before the pages created by sphinx.
# The format is a list of tuples containing the path and title.
#epub_pre_files = []

# HTML files shat should be inserted after the pages created by sphinx.
# The format is a list of tuples containing the path and title.
#epub_post_files = []

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['_static/opensearch.xml', '_static/doctools.js',
    '_static/jquery.js', '_static/searchtools.js', '_static/underscore.js',
    '_static/basic.css', 'search.html', '_static/websupport.js',
    '_branding', 'build', '_input']

# The depth of the table of contents in toc.ncx.
epub_tocdepth = 2

# Allow duplicate toc entries.
epub_tocdup = False

# Choose between 'default' and 'includehidden'.
epub_tocscope = 'default'

# Fix unsupported image types using the Pillow.
#epub_fix_images = False

# Scale large images.
epub_max_image_width = 0

# How to display URL addresses: 'footnote', 'no', or 'inline'.
epub_show_urls = 'footnote'

# If false, no index is generated.
epub_use_index = True

# -- Options for HTMLHelp output ------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename =  "%s" % project

bibtex_bibfiles = ['my-references.bib']

html_sourcelink_suffix = ''

# -- Options for PlantUML ------------------------------------------

sphinxmark_enable = True

# -- Options for bibtex ------------------------------------------

bibtex_bibfiles = ['refs.bib']

# -- Options for sphinx-needs ------------------------------------------
#
# https://sphinxcontrib-needs.readthedocs.io/en/latest/configuration.html
#
# Default: True
#
needs_include_needs = True

# needs_id_length¶
# This option defines the length of an automated generated ID (the length of the prefix does not count).
#
# Default: 5
#
needs_id_length = 5

#
# needs_types¶
# The option allows the setup of own need types like bugs, user_stories and more.
#
# By default it is set to:

needs_types = [dict(directive="req", title="Requirement", prefix="R_", color="#BFD8D2", style="node"),
               dict(directive="spec", title="Specification", prefix="S_", color="#FEDCD2", style="node"),
               dict(directive="impl", title="Implementation", prefix="I_", color="#DF744A", style="node"),
               dict(directive="test", title="Test Case", prefix="T_", color="#DCB239", style="node"),
               # Kept for backwards compatibility
               dict(directive="need", title="Need", prefix="N_", color="#9856a5", style="node")
           ]

# needs_types must be a list of dictionaries where each dictionary must contain the following items:
#
# directive: Name of the directive. For instance, you can use “req” via .. req:: in documents
#
# title: Title, used as human readable name in lists
# 
# prefix: A prefix for generated IDs, to easily identify that an ID belongs to a specific type. Can also be “”
#
# color: A color as hex value. Used in diagrams and some days maybe in other representations as well. Can also be “”
#
# style: A plantuml node type, like node, artifact, frame, storage or database. See plantuml documentation for more.

needs_extra_options = ['introduced', 'updated', 'impacts', 'main']

# -- Options for Napoleon ------------------------------------------
#
# https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
#
napoleon_google_docstring = True
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True

html_baseurl = '/documentation/'
