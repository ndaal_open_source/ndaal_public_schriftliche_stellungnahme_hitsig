.. raw:: latex

    \chapter*{Kontakt}
    \phantomsection
    \addcontentsline{toc}{chapter}{Kontakt}
    \vspace{1.5cm}

.. image::
   /imgs/contact.png
   :width: 86%
   :align: right


..
    Similar to 'cover.png' in conf.py, if it happens that an error occurs such that 'cover.png' is mentioned, please add the full path to the image beforehand. 

.. raw:: latex

        \fancyfoot[R]{\begin{tikzpicture}[remember picture,overlay]
    \draw  let \p1=($(current page footer area.north)-(current page.south)$),
          \n1={veclen(\x1,\y1)} in
    node [inner sep=0,outer sep=0,above right]
          at (current page.south west){\includegraphics[width=\paperwidth,height=2in]{/Users/cloud/repos/ndaal_public_schriftliche_stellungnahme_hitsig/documentation/source/cover.png}};
    \end{tikzpicture}}

.. raw:: latex

    \renewcommand{\headrulewidth}{0pt}

    \fancypagestyle{plain}{
    \lhead{}
    \fancyhead[R]{}
    \fancyhead[L]{}}
    \pagestyle{empty}


